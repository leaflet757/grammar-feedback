﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackInterface
{
    class ContextFreeWrapper
    {
        private string _stdout;

        public string STDOUT
        {
            get { return _stdout; }
            set { _stdout = value; }
        }

        private string _stderr;

        public string STDERR
        {
            get { return _stderr; }
            set { _stderr = value; }
        }


        public ContextFreeWrapper() { }

        public void Run(object[] data)
        {
            string CFGPath = (string) data[0];
            string CFGSize = (string) data[1];
            string Seed = (string) data[2];
            string Content = (string) data[3];

            // generate png file and shape datafile
            Process process = new Process();
            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = CFGPath;
            process.StartInfo.Arguments = "/s" + CFGSize.ToString() + " " + "/v " + Seed + " -" + " " + "out.png";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            process.StandardInput.Write(Content);
            process.StandardInput.Close();
            _stdout = process.StandardOutput.ReadToEnd();
            _stderr = process.StandardError.ReadToEnd();
            process.WaitForExit();// Waits here for the process to exit.
        }

    }
}
