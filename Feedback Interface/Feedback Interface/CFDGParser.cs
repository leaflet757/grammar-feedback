﻿using Sprache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackInterface
{
    public class Feature
    {
        public Feature(string name, float value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }
        public float Value { get; set; }

        public override string ToString()
        {
            return Name + " " + Value.ToString();
        }
    }
    
    public class Element 
    {
        public Element(string name, IEnumerable<Feature> elements) 
        {
            Name = name;
            Features = elements;
        }
        public IEnumerable<Feature> Features;
        public string Name;

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Name + " " + CFDGParser.ElementOpenChar + " ");
            foreach (Feature f in Features)
            {
                builder.Append(f + " ");
            }
            builder.Append(CFDGParser.ElementCloseChar);
            return builder.ToString();
        }

        public void AddFeature(string name, float value)
        {
            Features = Features.Concat<Feature>(new[] { new Feature(name, value) });
        }
    }

    // Rule
    public class Rule
    {
        public Rule(string id, int ruleIndex, IEnumerable<Element> elements, float rate)
        {
            ID = id;
            RuleIndex = ruleIndex;
            Elements = elements;
            Rate = rate;
        }

        public string ID { get; set; }
        public int RuleIndex { get; set; }
        public IEnumerable<Element> Elements { get; set; }
        public float Rate { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(ID + " " + Rate.ToString() + " {\n");
            foreach (Element i in Elements)
            {
                builder.Append("    " + i + "\n");
            }
            builder.Append("}");
            return builder.ToString();
        }
    }

    // Shape 
    public class Shape
    {
        public Shape(string id, string name, IEnumerable<Element> elements, float rate)
        {
            ID = id;
            Name = name;
            Elements = elements;
            Rate = rate;
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<Element> Elements { get; set; }
        public float Rate { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(ID + " " + Name + " " + Rate.ToString() + " {\n");
            foreach (Element i in Elements)
            {
                builder.Append("    " + i + "\n");
            }
            builder.AppendLine("}");
            return builder.ToString();
        }
    }

    // ShapeWithRules
    public class ShapeWithRules
    {
        public ShapeWithRules(string id, string name, IEnumerable<Rule> rules)
        {
            ID = id;
            Name = name;
            Rules = rules;
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<Rule> Rules { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(ID + " " + Name + " \n");
            foreach (Rule i in Rules)
            {
                builder.Append(i + "\n");
            }
            return builder.ToString();
        }
    }

    public interface Document
    {
        
    }

    // Document = Questionaire
    public class DocumentOld : Document
    {
        public DocumentOld(string id, string name, IEnumerable<Shape> shapes)
        {
            ID = id;
            StartShapeName = name;
            Shapes = shapes;
        }

        public string ID { get; set; }
        public string StartShapeName { get; set; }
        public IEnumerable<Shape> Shapes { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(ID + " " + StartShapeName + "\n");
            foreach (Shape s in Shapes) {
                builder.Append(s + "\n");
            }
            return builder.ToString();
        }
    }

    // Yes I am a bad programmer but you cant stop me!!!!
    public class DocumentNew : Document
    {
        public DocumentNew(string id, string name, IEnumerable<ShapeWithRules> shapes)
        {
            ID = id;
            StartShapeName = name;
            Shapes = shapes;
        }

        public string ID { get; set; }
        public string StartShapeName { get; set; }
        public IEnumerable<ShapeWithRules> Shapes { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(ID + " " + StartShapeName + "\n");
            foreach (ShapeWithRules s in Shapes)
            {
                builder.Append(s + "\n");
            }
            return builder.ToString();
        }
    }

    public enum Version
    {
        New,
        Old
    }

    public static class CFDGParser
    {
        private static Version _versionType = Version.New;
        public static Version VersionType
        {
            get 
            {
                return _versionType; 
            }
            set 
            {
                if (value == Version.Old)
                {
                    _elementOpenChar = '{';
                    _elementCloseChar = '}';
                }
                else
                {
                    _elementOpenChar = '[';
                    _elementCloseChar = ']';
                }
                _versionType = value; 
            }
        }

        private static char _elementOpenChar = '[';
        public static char ElementOpenChar
        {
            get { return _elementOpenChar; }
            //set { _elementOpenChar = value; }
        }

        private static char _elementCloseChar = ']';
        public static char ElementCloseChar
        {
            get { return _elementCloseChar; }
            //set { _elementCloseChar = value; }
        }

        private static string _identifierStartShape = "startshape";
        public static string IDENTIFIER_STARTSHAPE
        {
            get { return _identifierStartShape; }
        }

        private static string _identifierShape = "shape";
        public static string IDENTIFIER_SHAPE
        {
            get { return _identifierShape; }
        }

        private static string _identifierRule = "rule";
        public static string IDENTIFIER_RULE
        {
            get { return _identifierRule; }
        }


        private static IEnumerable<char> StopChars = new char[] { ' ', '[', '{', '('};
        
        // Parse Helper Elements
        public static CommentParser Comment = new CommentParser("#", "/*", "*/", "\r\n");

        public static readonly Parser<string> Identifier = Parse.LetterOrDigit.AtLeastOnce().Text().Token();
        //public static readonly Parser<string> Identifier = Parse.AnyChar.Except(WhiteSpace).AtLeastOnce().Text().Token();
        public static readonly Parser<string> WhiteSpace = Parse.WhiteSpace.AtLeastOnce().Text().Token();

        //public static readonly Parser<string> Identifier = Parse.AnyChar.DelimitedBy(WhiteSpace).Text().Token();

        //public static readonly Parser<string> Identifier =
        //    from leading in Parse.WhiteSpace.Many()
        //    from name in Parse.CharExcept(StopChars).Many().Text().Token() // Parse.AnyChar.Many().Text().Token();
        //    from trailing in Parse.WhiteSpace.Many()
        //    select name;

        public static readonly Parser<Feature> Feature =
            //from lbracket in Parse.Char('[')
            from name in Identifier
            from op in Parse.Optional(Parse.Char('-').Token())
            from val in Parse.Decimal
            //from rbracket in Parse.Char(']')
            select new Feature(name, float.Parse(val) * (op.IsDefined ? -1 : 1));

        public static readonly Parser<Element> Element =
            from name in Identifier
            from lbracket in Parse.Char(_elementOpenChar).Token()
            from feats in Feature.Many()
            from rbracket in Parse.Char(_elementCloseChar).Token()
            select new Element(name, feats);

        // Rules for newer cfdg version
        public static readonly Parser<Rule> Rule =
            from id in Identifier
            from rate in Parse.Optional(Parse.Decimal)
            from lbracket in Parse.Char('{').Token()
            from elements in Element.Many()
            from rbracket in Parse.Char('}').Token()
            where id.ToLower() == IDENTIFIER_RULE
            select new Rule(id, 0, elements, (rate.IsDefined ? float.Parse(rate.Get()) : 1f));

        // Shape = Section
        public static readonly Parser<Shape> Shape =
            from id in Identifier
            from name in Identifier
            from rate in Parse.Optional(Parse.Decimal)
            from lbracket in Parse.Char('{').Token()
            from elements in Element.Many()
            from rbracket in Parse.Char('}').Token()
            where id.ToLower() == IDENTIFIER_RULE
            select new Shape(id, name, elements, (rate.IsDefined ? float.Parse(rate.Get()) : 1f));


        // Shape = Section
        public static readonly Parser<ShapeWithRules> ShapeWithRules =
            from id in Identifier
            from name in Identifier
            from rules in Rule.Many()
            where id.ToLower() == IDENTIFIER_SHAPE
            select new ShapeWithRules(id, name, rules);

        // Document = Questionaire
        public static Parser<DocumentOld> DocumentOld =
            from id in Identifier
            from name in Identifier
            from shapes in Shape.Many()
            where id.ToLower() == IDENTIFIER_STARTSHAPE
            select new DocumentOld(id, name, shapes);
            //Shape.Many().Select(shapes => new Document(id, name, shapes)).End();

        // Newer Context Free
        public static Parser<DocumentNew> DocumentNew =
            from id in Identifier
            from name in Identifier
            from shapes in ShapeWithRules.Many()
            where id.ToLower() == IDENTIFIER_STARTSHAPE
            select new DocumentNew(id, name, shapes);


        // THE ONLY FUNCTION YIOU SHOULD EVER CALL
        public static Document ParseDocument(string input)
        {
            if (_versionType == Version.Old)
            {
                return DocumentOld.Parse(input);
            }
            else
            {
                return DocumentNew.Parse(input);
            }
        }
    }
}
