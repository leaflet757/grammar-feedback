﻿using System.Collections.Generic;
using System.IO;
namespace FeedbackInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renderFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxEditor = new System.Windows.Forms.RichTextBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.histogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSummerizor = new System.Windows.Forms.Label();
            this.textBoxSummerizor = new System.Windows.Forms.TextBox();
            this.textBoxSize = new System.Windows.Forms.TextBox();
            this.labelSize = new System.Windows.Forms.Label();
            this.prefDoneButton = new System.Windows.Forms.Button();
            this.textboxCFG = new System.Windows.Forms.TextBox();
            this.cfgLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxVersionSelector = new System.Windows.Forms.ComboBox();
            this.buttonRender = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBoxSeed = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.textBoxSTDERR = new System.Windows.Forms.RichTextBox();
            this.pictureBoxAverage = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogram)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAverage)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(972, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.openFileToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.renderFileToolStripMenuItem,
            this.preferencesToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newFileToolStripMenuItem
            // 
            this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
            this.newFileToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.newFileToolStripMenuItem.Text = "New File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.openFileToolStripMenuItem.Text = "Open File";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            // 
            // renderFileToolStripMenuItem
            // 
            this.renderFileToolStripMenuItem.Name = "renderFileToolStripMenuItem";
            this.renderFileToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.renderFileToolStripMenuItem.Text = "Render File";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.preferencesToolStripMenuItem.Text = "Preferences";
            // 
            // textBoxEditor
            // 
            this.textBoxEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEditor.Location = new System.Drawing.Point(0, 24);
            this.textBoxEditor.Name = "textBoxEditor";
            this.textBoxEditor.Size = new System.Drawing.Size(381, 458);
            this.textBoxEditor.TabIndex = 1;
            this.textBoxEditor.Text = "";
            this.textBoxEditor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.textBoxEditor_MouseDown);
            this.textBoxEditor.MouseMove += new System.Windows.Forms.MouseEventHandler(this.textBoxEditor_MouseMove);
            this.textBoxEditor.MouseUp += new System.Windows.Forms.MouseEventHandler(this.textBoxEditor_MouseUp);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Location = new System.Drawing.Point(381, 24);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(590, 655);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            // 
            // histogram
            // 
            this.histogram.BackColor = System.Drawing.Color.Silver;
            chartArea2.Name = "ChartArea1";
            this.histogram.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.histogram.Legends.Add(legend2);
            this.histogram.Location = new System.Drawing.Point(5, 485);
            this.histogram.Name = "histogram";
            this.histogram.Size = new System.Drawing.Size(371, 190);
            this.histogram.TabIndex = 3;
            this.histogram.Text = "chart1";
            this.histogram.MouseMove += new System.Windows.Forms.MouseEventHandler(this.histogram_MouseMove);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelSummerizor);
            this.panel1.Controls.Add(this.textBoxSummerizor);
            this.panel1.Controls.Add(this.textBoxSize);
            this.panel1.Controls.Add(this.labelSize);
            this.panel1.Controls.Add(this.prefDoneButton);
            this.panel1.Controls.Add(this.textboxCFG);
            this.panel1.Controls.Add(this.cfgLabel);
            this.panel1.Location = new System.Drawing.Point(436, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(306, 238);
            this.panel1.TabIndex = 4;
            this.panel1.Visible = false;
            // 
            // labelSummerizor
            // 
            this.labelSummerizor.AutoSize = true;
            this.labelSummerizor.Location = new System.Drawing.Point(21, 80);
            this.labelSummerizor.Name = "labelSummerizor";
            this.labelSummerizor.Size = new System.Drawing.Size(46, 13);
            this.labelSummerizor.TabIndex = 7;
            this.labelSummerizor.Text = "SumLoc";
            // 
            // textBoxSummerizor
            // 
            this.textBoxSummerizor.Location = new System.Drawing.Point(85, 77);
            this.textBoxSummerizor.Name = "textBoxSummerizor";
            this.textBoxSummerizor.Size = new System.Drawing.Size(192, 20);
            this.textBoxSummerizor.TabIndex = 6;
            // 
            // textBoxSize
            // 
            this.textBoxSize.Location = new System.Drawing.Point(85, 118);
            this.textBoxSize.Name = "textBoxSize";
            this.textBoxSize.Size = new System.Drawing.Size(192, 20);
            this.textBoxSize.TabIndex = 4;
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.Location = new System.Drawing.Point(40, 121);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(30, 13);
            this.labelSize.TabIndex = 3;
            this.labelSize.Text = "Size:";
            // 
            // prefDoneButton
            // 
            this.prefDoneButton.Location = new System.Drawing.Point(218, 202);
            this.prefDoneButton.Name = "prefDoneButton";
            this.prefDoneButton.Size = new System.Drawing.Size(75, 23);
            this.prefDoneButton.TabIndex = 2;
            this.prefDoneButton.Text = "Done";
            this.prefDoneButton.UseVisualStyleBackColor = true;
            this.prefDoneButton.Click += new System.EventHandler(this.prefDoneButton_Click);
            // 
            // textboxCFG
            // 
            this.textboxCFG.Location = new System.Drawing.Point(85, 37);
            this.textboxCFG.Name = "textboxCFG";
            this.textboxCFG.Size = new System.Drawing.Size(192, 20);
            this.textboxCFG.TabIndex = 1;
            // 
            // cfgLabel
            // 
            this.cfgLabel.AutoSize = true;
            this.cfgLabel.Location = new System.Drawing.Point(21, 40);
            this.cfgLabel.Name = "cfgLabel";
            this.cfgLabel.Size = new System.Drawing.Size(49, 13);
            this.cfgLabel.TabIndex = 0;
            this.cfgLabel.Text = "CFGLoc:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(68, 563);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 22);
            this.label2.TabIndex = 5;
            this.label2.Text = "No rule under selected shape.";
            this.label2.Visible = false;
            // 
            // comboBoxVersionSelector
            // 
            this.comboBoxVersionSelector.FormattingEnabled = true;
            this.comboBoxVersionSelector.Items.AddRange(new object[] {
            "New Version",
            "Old Version"});
            this.comboBoxVersionSelector.Location = new System.Drawing.Point(72, 3);
            this.comboBoxVersionSelector.Name = "comboBoxVersionSelector";
            this.comboBoxVersionSelector.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVersionSelector.TabIndex = 6;
            // 
            // buttonRender
            // 
            this.buttonRender.Location = new System.Drawing.Point(243, 1);
            this.buttonRender.Name = "buttonRender";
            this.buttonRender.Size = new System.Drawing.Size(75, 23);
            this.buttonRender.TabIndex = 7;
            this.buttonRender.Text = "Render";
            this.buttonRender.UseVisualStyleBackColor = true;
            this.buttonRender.Click += new System.EventHandler(this.buttonRender_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // textBoxSeed
            // 
            this.textBoxSeed.Location = new System.Drawing.Point(460, 3);
            this.textBoxSeed.Name = "textBoxSeed";
            this.textBoxSeed.Size = new System.Drawing.Size(105, 20);
            this.textBoxSeed.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(355, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Random Seed";
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Location = new System.Drawing.Point(579, 3);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(27, 18);
            this.hScrollBar1.TabIndex = 10;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // textBoxSTDERR
            // 
            this.textBoxSTDERR.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBoxSTDERR.Location = new System.Drawing.Point(381, 651);
            this.textBoxSTDERR.Name = "textBoxSTDERR";
            this.textBoxSTDERR.ReadOnly = true;
            this.textBoxSTDERR.Size = new System.Drawing.Size(71, 27);
            this.textBoxSTDERR.TabIndex = 11;
            this.textBoxSTDERR.Text = "";
            this.textBoxSTDERR.MouseEnter += new System.EventHandler(this.textBoxSTDERR_MouseEnter);
            this.textBoxSTDERR.MouseLeave += new System.EventHandler(this.textBoxSTDERR_MouseLeave);
            // 
            // pictureBoxAverage
            // 
            this.pictureBoxAverage.Location = new System.Drawing.Point(901, 653);
            this.pictureBoxAverage.Name = "pictureBoxAverage";
            this.pictureBoxAverage.Size = new System.Drawing.Size(70, 26);
            this.pictureBoxAverage.TabIndex = 12;
            this.pictureBoxAverage.TabStop = false;
            this.pictureBoxAverage.MouseEnter += new System.EventHandler(this.pictureBoxAverage_MouseEnter);
            this.pictureBoxAverage.MouseLeave += new System.EventHandler(this.pictureBoxAverage_MouseLeave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 679);
            this.Controls.Add(this.pictureBoxAverage);
            this.Controls.Add(this.textBoxSTDERR);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSeed);
            this.Controls.Add(this.buttonRender);
            this.Controls.Add(this.comboBoxVersionSelector);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.histogram);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.textBoxEditor);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.histogram)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAverage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void saveAsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            SaveAsFile();
        }

        void renderFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            RunContextFree();
        }

        void openFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            OpenFileCFDG();
        }

        void newFileToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            textBoxEditor.Clear();
        }

        void preferencesToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            this.textboxCFG.Text = CFGPath;
            this.textBoxSize.Text = CFGSize.ToString();
            this.panel1.Visible = true;
        }

        private void prefDoneButton_Click(object sender, System.EventArgs e)
        {
            List<string> items = new List<string>();
            items.Add(textboxCFG.Text);
            items.Add(textBoxSize.Text);

            SaveCFGSettings(items);
            LoadCFGSettings();

            this.panel1.Visible = false;
        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renderFileToolStripMenuItem;
        private System.Windows.Forms.RichTextBox textBoxEditor;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart histogram;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label cfgLabel;
        private System.Windows.Forms.TextBox textboxCFG;
        private System.Windows.Forms.Button prefDoneButton;
        private System.Windows.Forms.TextBox textBoxSize;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxVersionSelector;
        private System.Windows.Forms.Button buttonRender;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelSummerizor;
        private System.Windows.Forms.TextBox textBoxSummerizor;
        private System.Windows.Forms.TextBox textBoxSeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.RichTextBox textBoxSTDERR;
        private System.Windows.Forms.PictureBox pictureBoxAverage;
    }
}

