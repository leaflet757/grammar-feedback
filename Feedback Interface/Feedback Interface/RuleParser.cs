﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackInterface
{
    class ShapeInfo
    {
        public string Name;
        public int Count;
        public Dictionary<int, int> rules;
    }

    class RuleParser
    {
        private List<ShapeInfo> _shapes;
        public List<ShapeInfo> Shapes
        {
            get { return _shapes; }
        }

        // Key words
        const string RULE = "rule";
        const string SHAPE = "shape";
        
        // Key Symbols
        const char RULE_IDICATOR = 'z';

        public RuleParser()
        {
            _shapes = new List<ShapeInfo>();
        }

        public ShapeInfo GetShapeInfo(int index)
        {
            return _shapes[index];
        }

        public void Parse(string text, bool useNew = true)
        {
            if (useNew)
            {
                Scanner scanner = new Scanner(text);

                ShapeInfo item = null;
                bool recordShapeName = false;
                bool recordRules = false;

                int totalShapes = 0;
                int ruleCount = 0;

                // TEMP
                Random r = new Random();

                string prev = "";
                int c = 0;
                // SS DankMemes 
                while (scanner.hasNext())
                {
                    c++;
                    string str = scanner.Next();

                    if (recordShapeName)
                    {
                        recordShapeName = false;
                        item.Name = str;
                        recordRules = true;
                        if (item != null)
                        {
                            item.Count = r.Next(1, 10); // TODO: remove later.
                            _shapes.Add(item);
                        }
                    }
                    if (recordRules && str.Equals(RULE))
                    {
                        item.rules.Add(ruleCount++, r.Next(1, 10)); // TODO: remove later
                    }
                    if (str.Equals(SHAPE))
                    {
                        recordRules = false;
                        ruleCount = 0;
                        totalShapes++;
                        item = new ShapeInfo();
                        item.rules = new Dictionary<int, int>();
                        recordShapeName = true;
                    }

                    prev = str;
                }
            }
            else
            {
                Scanner scanner = new Scanner(text);

                ShapeInfo item = null;
                bool recordShapeName = false;

                int totalShapes = 0;

                // TEMP
                Random r = new Random();

                string prev = "";
                int c = 0;

                while (scanner.hasNext())
                {
                    c++;
                    string str = scanner.Next();

                    if (recordShapeName)
                    {
                        recordShapeName = false;
                        item.Name = str;
                        if (item != null)
                        {
                            item.Count = r.Next(1, 10); // TODO: remove later.
                            _shapes.Add(item);
                        }
                    }
                    if (str.Equals(RULE))
                    {
                        totalShapes++;
                        item = new ShapeInfo();
                        item.rules = new Dictionary<int, int>();
                        recordShapeName = true;
                    }
                    prev = str;
                }
            }
        }

        internal ShapeInfo GetShapeInfoFromName(string selectedSeries)
        {
            foreach (ShapeInfo item in Shapes)
            {
                if (item.Name == selectedSeries)
                {
                    return item;
                }
            }
            return null;
        }

        internal void ReadShapeDataText(string text, bool usingNewVersion)
        {
            Scanner scanner = new Scanner(text);

            while (scanner.hasNext())
            {
                string str = scanner.Next();

                // pseudo rule shape
                if (str.Length > 1 && str[0] == RULE_IDICATOR && str[1] == RULE_IDICATOR)
                {
                    FindShapeByRuleName(str, int.Parse(scanner.Next()));
                }
                else // actual shape
                {
                    ShapeInfo item = new ShapeInfo();
                    item.rules = new Dictionary<int, int>();
                    item.Name = str;
                    item.Count = int.Parse(scanner.Next());
                    _shapes.Add(item);
                }
            }
        }

        private void FindShapeByRuleName(string srcStr, int ruleCount)
        {
            string ss = srcStr.Substring(2);
            string name = ss.Substring(0, ss.IndexOf(RULE_IDICATOR));
            int number = int.Parse(ss.Substring(ss.IndexOf(RULE_IDICATOR)+1));


            ShapeInfo item = null;
            foreach (ShapeInfo i in _shapes)
            {
                if (i.Name == name)
                {
                    item = i;
                }
            }

            item.rules.Add(number, ruleCount);
        }

        internal void Clear()
        {
            _shapes.Clear();
        }
    }
}
