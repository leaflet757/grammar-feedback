﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FeedbackInterface
{
    struct HilightedAttribute
    {
        public float Value;
        public int StartIndex;
        public int Length;
    }

    public partial class Form1 : Form
    {
        private const string preferencesFileName = "cfgsettings.txt";

        private const string AVERAGES_DIRECTORY = "temp\\";

        private const string OUTPUT_FILENAME = "out.png";

        private string _cfgPath = "";
        public string CFGPath
        {
            get { return _cfgPath; }

            set
            {
                if (_cfgPath.Equals(""))
                {
                    _cfgPath = value;
                }
            }
        }

        private string _summerizorPath;
        public string SummerizorPath
        {
            get { return _summerizorPath; }
            set { _summerizorPath = value; }
        }

        private string _cfsrc = "";
        public string CFSRC
        {
            get { return _cfsrc; }
            set { _cfsrc = value; }
        }

        private bool _CursorShown = true;
        public bool CursorShown
        {
            get
            {
                return _CursorShown;
            }
            set
            {
                if (value == _CursorShown)
                {
                    return;
                }

                if (value)
                {
                    System.Windows.Forms.Cursor.Show();
                }
                else
                {
                    System.Windows.Forms.Cursor.Hide();
                }

                _CursorShown = value;
            }
        }

        private int CFGSize = 100;
        public const float INCREMENT = 0.01f;
        private Random random = new Random();
        private int currentSeedIndex = -1;
        private List<string> seedList = new List<string>();

        private RuleParser parserRuleChart;

        private string selectedSeries = "";
        private HilightedAttribute selectedAttribute;

        private bool usingRules = false;
        private bool controlDown = false;
        private bool clickDown = false;

        private Thread renderThread;
        private ContextFreeWrapper cfwrap;

        // stuff about the expanding controls
        private Size outputSizeBig;
        private Size averageSizeBig;
        private Point outputLocBig;
        private Point averageLocBig;
        private Size outputSizeSmall;
        private Size averageSizeSmall;
        private Point outputLocSmall;
        private Point averageLocSmall;

        public Form1(string filePath)
        {
            InitializeComponent();

            parserRuleChart = new RuleParser();

            // WHY THE HELL DID THE BUTTONS KEEP DELETING THEMSELVES
            this.preferencesToolStripMenuItem.Click += preferencesToolStripMenuItem_Click;
            this.openFileToolStripMenuItem.Click += openFileToolStripMenuItem_Click;
            this.saveAsToolStripMenuItem.Click += saveAsToolStripMenuItem_Click;
            this.newFileToolStripMenuItem.Click += newFileToolStripMenuItem_Click;
            this.renderFileToolStripMenuItem.Click += renderFileToolStripMenuItem_Click;
            this.textBoxEditor.KeyDown += textBoxEditor_KeyDown;
            this.textBoxEditor.KeyUp += textBoxEditor_KeyUp;

            if (!File.Exists(preferencesFileName))
            {
                File.CreateText(preferencesFileName);
            }

            this.DesktopLocation = new Point(0, 0);
            this.comboBoxVersionSelector.SelectedIndex = 0;

            LoadCFGSettings();
            using (FileStream stream = File.Open(filePath, FileMode.Open))
            {
                TextReader tr = new StreamReader(stream);
                textBoxEditor.Text = tr.ReadToEnd();
            }
            
            timer1.Enabled = true;
            timer1.Start();

            // load an image when starting the program
            cfwrap = new ContextFreeWrapper();
            //renderThread = new Thread(new ParameterizedThreadStart(cfwrap.Test));

            // init expanding controls
            outputSizeBig = new Size(383, 200);
            averageSizeBig = new Size(200, 200);
            outputLocBig = new Point(382, 479);
            averageLocBig = new Point(771, 479);
            outputSizeSmall = new Size(textBoxSTDERR.Size.Width, textBoxSTDERR.Size.Height);
            averageSizeSmall = new Size(pictureBoxAverage.Size.Width, pictureBoxAverage.Size.Height);
            outputLocSmall = new Point(textBoxSTDERR.Location.X, textBoxSTDERR.Location.Y);
            averageLocSmall = new Point(pictureBoxAverage.Location.X, pictureBoxAverage.Location.Y);
            textBoxSTDERR.Text = "stdout";

            RunContextFree();
        }

        private void OpenFileCFDG()
        {
            histogram.Series.Clear();

            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "cdfg files (*.cfdg)|*.cfdg|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        _cfsrc = openFileDialog1.FileName;
                        using (myStream)
                        {
                            TextReader tr = new StreamReader(myStream);
                            textBoxEditor.Text = tr.ReadToEnd();
                            tr.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void SaveAsFile()
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "cdfg files (*.cfdg)|*.cfdg|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    StreamWriter sw = new StreamWriter(myStream);
                    Console.WriteLine(textBoxEditor.Text);
                    sw.Write(textBoxEditor.Text);
                    sw.Flush();
                    sw.Dispose();
                    myStream.Close();
                }
            }
        }

        private void LoadCFGSettings()
        {
            // Open the stream and read it back. 
            using (StreamReader sr = File.OpenText(preferencesFileName))
            {
                string s = "";
                int line = 0;
                while ((s = sr.ReadLine()) != null)
                {
                    switch (line)
                    {
                        case 0: // cfg path
                            CFGPath = s;
                            break;
                        case 1:
                            CFGSize = int.Parse(s);
                            break;
                        case 2:
                            SummerizorPath = s;
                            break;
                        case 3:
                            //textBoxEditor.Text = 
                            break;
                        case 4:
                            break;
                    }
                    line++;
                }
            }
        }

        private void SaveCFGSettings(List<string> items)
        {
            File.Delete(preferencesFileName); // because im lazy
            // Create a file to write to. 
            using (StreamWriter sw = File.CreateText(preferencesFileName))
            {
                for (int i = 0; i < items.Count; i++)
                {
                    sw.WriteLine(items[i]);
                }
            }
        }

        private void RunContextFree()
        {
            // clear chart of any previous renderings
            histogram.Series.Clear();
            parserRuleChart.Clear();
            UpdateChart();

            if (changeSeed)
            {
                seedList.Add(RandomString(3));
                currentSeedIndex++;
            }
            changeSeed = true;
            textBoxSeed.Text = seedList[currentSeedIndex];

            // run context free
            string input = textBoxEditor.Text;
            string selected = textBoxEditor.SelectedText;
            // parse text
            string[] data = { CFGPath, CFGSize.ToString(), textBoxSeed.Text, input };
            cfwrap.Run(data);
            //MessageBox.Show(cfwrap.STDERR);

            // read shape data file
            string path = Path.GetDirectoryName(CFGPath);
            string dataFilePath = "shape_data"; //path + Path.DirectorySeparatorChar + "shape_data";

            StringBuilder builder = new StringBuilder();
            using (StreamReader sr = File.OpenText(dataFilePath))
            {
                string s = "";
                int line = 0;
                while ((s = sr.ReadLine()) != null)
                {
                    builder.AppendLine(s);
                    line++;
                }
            }

            if (comboBoxVersionSelector.SelectedIndex == 0)
            {
                // new version
                parserRuleChart.ReadShapeDataText(builder.ToString(), true);
                CFDGParser.VersionType = Version.New;
                var parsed = CFDGParser.ParseDocument(textBoxEditor.Text);
                DocumentNew doc = (DocumentNew)parsed;
            }
            else
            {
                // old version
                parserRuleChart.ReadShapeDataText(builder.ToString(), false);
                CFDGParser.VersionType = Version.Old;
                var parsed = CFDGParser.ParseDocument(textBoxEditor.Text);
                DocumentOld doc = (DocumentOld)parsed;
            }

            UpdateChart();
            DisplayImage();
            //MessageBox.Show(textBoxEditor.SelectedText);

            // Create smaller heatmap image
            using (Image heatImage = GetCopyImage(OUTPUT_FILENAME, 100))
            {
                if (!Directory.Exists(AVERAGES_DIRECTORY))
                {
                    Directory.CreateDirectory(AVERAGES_DIRECTORY);
                }
                heatImage.Save(AVERAGES_DIRECTORY + "average.png");
            }
            // continiously generate images
            AverageGenerations();
        }

        private void AverageGenerations()
        {
            for (int i = 0; i < 8; i++)
            {
                Process process = new Process();
                process.StartInfo.FileName = "CF Summerizor.exe";
                process.StartInfo.Arguments = string.Format("{0} {1} \"{2}\" {3}", CFGPath, 200, textBoxEditor.Text, AVERAGES_DIRECTORY);
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                process.WaitForExit();

                /*
                 * "B:\Programs_32\OzoneSoft\ContextFree\ContextFreeCLI.exe" 100 "B:\Documents\GitHub\grammar-feedback\ContextFree\Simple_Tree2.cfdg" "B:\Documents\GitHub\grammar-feedback\ContextFree\OutputImages" 1000
                 */
            }
        }

        private void DisplayImage()
        {
            try
            {
                pictureBox.Image = GetCopyImage(OUTPUT_FILENAME, CFGSize);
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not load the generated image. \n" + e.ToString());
            }
        }

        private Image GetCopyImage(string path, int size)
        {
            using (Image im = Image.FromFile(path))
            {
                Bitmap bm = new Bitmap(im, size, size);
                return bm;
            }
        }

        private void UpdateChart()
        {
            for (int i = 0; i < parserRuleChart.Shapes.Count; i++)
            {
                ShapeInfo si = parserRuleChart.GetShapeInfo(i);

                System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();

                series1.ChartArea = "ChartArea1";
                series1.Legend = "Legend1";
                series1.Name = si.Name;

                bool repeat = true;
                int t = 2;
                while (repeat)
                {
                    try
                    {
                        this.histogram.Series.Add(series1);
                        series1.Points.Add(si.Count);
                        repeat = false;
                    }
                    catch (ArgumentException e)
                    {
                        series1.Name = si.Name + t.ToString();
                        t++;
                    }
                }
            }
            this.histogram.Update();
        }

        private void histogram_MouseMove(object sender, MouseEventArgs e)
        {
            selectedSeries = "";

            // Call HitTest
            HitTestResult result = histogram.HitTest(e.X, e.Y);

            // Reset Data Point Attributes
            foreach (Series s in histogram.Series)
            {
                DataPoint point = s.Points[0];
                point.BackSecondaryColor = Color.Black;
                point.BackHatchStyle = ChartHatchStyle.None;
                point.BorderWidth = 1;
            }

            // If the mouse if over a data point
            //if (result.ChartElementType == ChartElementType.DataPoint)
            if (result.ChartElementType == ChartElementType.LegendItem)
            {
                // Find selected data point
                DataPoint point = result.Series.Points[0];

                selectedSeries = result.Series.Name;

                // Change the appearance of the data point
                point.BackSecondaryColor = Color.White;
                point.BackHatchStyle = ChartHatchStyle.Percent25;
                point.BorderWidth = 2;
            }
            else
            {
                // Set default cursor
                this.Cursor = Cursors.Default;
            }
        }

        void textBoxEditor_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
            {
                controlDown = true;
                if (selectedSeries.Length != 0 && !usingRules)
                {
                    // check if the user is selecting a series
                    checkChartSelection();
                }
            }

            if (e.KeyCode == Keys.F5)
            {
                // TODO: add stderr output and summery image if mouse hovered over bottom left or right of picture box
                RunContextFree();
            }
        }

        void textBoxEditor_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) != Keys.Control)
            {
                controlDown = false;
                usingRules = false;
                clickDown = false;
                CursorShown = true;
                label2.Visible = false;
                selectedAttribute.Value = float.NaN;
                histogram.Series.Clear();
                UpdateChart();
            }
        }

        private void checkChartSelection()
        {
            ShapeInfo info = parserRuleChart.GetShapeInfoFromName(selectedSeries);
            if (info != null)
            {
                usingRules = true;
                histogram.Series.Clear();

                //for (int i = 0; i < info.rules.Keys.Count; i++)
                foreach (KeyValuePair<int, int> pair in info.rules)
                {
                    //int count = info.rules[i];
                    int count = pair.Value;

                    System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();

                    series1.ChartArea = "ChartArea1";
                    series1.Legend = "Legend1";
                    series1.Name = string.Format("Rule {0}", pair.Key);
                    this.histogram.Series.Add(series1);
                    series1.Points.Add(count);
                }
                if (info.rules.Count == 0)
                {
                    label2.Visible = true;
                }
                this.histogram.Update();
            }
        }

        private void textBoxEditor_MouseMove(object sender, MouseEventArgs e)
        {
            if (controlDown)
            {
                if (!(sender is RichTextBox)) return;
                var targetTextBox = sender as RichTextBox;
                if (targetTextBox.TextLength < 1) return;

                var currentTextIndex = targetTextBox.GetCharIndexFromPosition(e.Location);

                var wordRegex = new Regex(@"(\w+)");
                var words = wordRegex.Matches(targetTextBox.Text);
                if (words.Count < 1) return;

                var currentWord = string.Empty;
                for (var i = words.Count - 1; i >= 0; i--)
                {
                    if (words[i].Index <= currentTextIndex)
                    {
                        currentWord = words[i].Value;
                        break;
                    }
                }
                // check if the word should really be selected
                if (currentWord == string.Empty) return;
                // find lower higlight bound
                char c = 't';
                while (!Char.IsWhiteSpace(c) && currentTextIndex > 0)
                {
                    c = textBoxEditor.Text[--currentTextIndex];
                }
                if (Char.IsWhiteSpace(c)) currentTextIndex++;
                // find upper highlight bound
                c = textBoxEditor.Text[currentTextIndex];
                int length = 0;
                while (!Char.IsWhiteSpace(c) && (currentTextIndex + length) < textBoxEditor.Text.Length &&
                    c != ']' && c != '}')
                {
                    c = textBoxEditor.Text[currentTextIndex + ++length];
                }
                //if (Char.IsWhiteSpace(c) || c == ']' || c == '}') length--;
                currentWord = textBoxEditor.Text.Substring(currentTextIndex, length);
                float f;
                if (!float.TryParse(currentWord, out f)) return;
                selectedAttribute.Value = f;
                selectedAttribute.Length = length;
                selectedAttribute.StartIndex = currentTextIndex;
                targetTextBox.Select(currentTextIndex, length);
            }
            if (clickDown)
            {
                CursorShown = false;
                currentMouse.X = System.Windows.Forms.Cursor.Position.X;
                currentMouse.Y = System.Windows.Forms.Cursor.Position.Y;

                bool valChange = false;

                if (currentMouse.X > prevMouse.X)
                {
                    valChange = true;
                    selectedAttribute.Value += INCREMENT;
                }
                if (currentMouse.X < prevMouse.X)
                {
                    valChange = true;
                    selectedAttribute.Value -= INCREMENT;
                }
                if (valChange)
                {
                    textBoxEditor.Text = textBoxEditor.Text.Substring(0, selectedAttribute.StartIndex)
                        + selectedAttribute.Value.ToString()
                        + textBoxEditor.Text.Substring(selectedAttribute.StartIndex
                        + selectedAttribute.Length);
                    selectedAttribute.Length = selectedAttribute.Value.ToString().Length;
                    RunContextFree();
                }
                System.Windows.Forms.Cursor.Position = new Point(prevMouse.X, prevMouse.Y);
            }

        }

        private void buttonRender_Click(object sender, EventArgs e)
        {
            RunContextFree();
        }

        private Point prevMouse = new Point();
        private Point currentMouse = new Point();
        private void textBoxEditor_MouseDown(object sender, MouseEventArgs e)
        {
            if (!clickDown)
            {
                prevMouse.X = System.Windows.Forms.Cursor.Position.X;
                prevMouse.Y = System.Windows.Forms.Cursor.Position.Y;
            }

            // if control is down and the selected attribute has a valid value
            if (controlDown && selectedAttribute.Value != float.NaN)
            {
                clickDown = true;
                //prevMouse.X = System.Windows.Forms.Cursor.Position.X;
                //prevMouse.Y = System.Windows.Forms.Cursor.Position.Y;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void textBoxEditor_MouseUp(object sender, MouseEventArgs e)
        {
            clickDown = false; // NOTE: this probably shouldn't go here
            CursorShown = true; // same with this
        }

        private string RandomString(int Size)
        {
            string input = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < Size; i++)
            {
                ch = input[random.Next(0, input.Length)];
                builder.Append(ch);
            }
            return builder.ToString();
        }

        private bool changeSeed = true;
        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.Type == ScrollEventType.SmallDecrement && currentSeedIndex > 0)
            {
                changeSeed = false;
                currentSeedIndex--;
            }
            if (e.Type == ScrollEventType.SmallIncrement && currentSeedIndex < seedList.Count - 1)
            {
                changeSeed = false;
                currentSeedIndex++;
            }
            textBoxSeed.Text = seedList[currentSeedIndex];
        }

        private void textBoxSTDERR_MouseEnter(object sender, EventArgs e)
        {
            textBoxSTDERR.Location = new Point(outputLocBig.X, outputLocBig.Y);
            textBoxSTDERR.Size = new Size(outputSizeBig.Width, outputSizeBig.Height);
            textBoxSTDERR.Text = cfwrap.STDERR;
        }

        private void textBoxSTDERR_MouseLeave(object sender, EventArgs e)
        {
            textBoxSTDERR.Location = new Point(outputLocSmall.X, outputLocSmall.Y);
            textBoxSTDERR.Size = new Size(outputSizeSmall.Width, outputSizeSmall.Height);
            textBoxSTDERR.Text = "stdout";
        }

        private void pictureBoxAverage_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxAverage.Location = new Point(averageLocBig.X, averageLocBig.Y);
            pictureBoxAverage.Size = new Size(averageSizeBig.Width, averageSizeBig.Height);
            pictureBoxAverage.Image = GetCopyImage(AVERAGES_DIRECTORY + "heatmap.png", 200);
        }

        private void pictureBoxAverage_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxAverage.Location = new Point(averageLocSmall.X, averageLocSmall.Y);
            pictureBoxAverage.Size = new Size(averageSizeSmall.Width, averageSizeSmall.Height);
            pictureBoxAverage.Image = null;
        }


    }
}
