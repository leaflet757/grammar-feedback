﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;

namespace CF_Summerizor
{
    class ImageHelper
    {
        // creating images
        private int start;
        private int end;
        private string cfLoc;
        private string imageSize;
        private string srcFile;
        private string outLoc;
        private int totalImageCount;

        // reading images
        private int[,] r, b, g;

        public ImageHelper(int start, int end, string[] args)
        {
            this.start = start;
            this.end = end;

            // cfloc
            cfLoc = args[0];
            // image size
            imageSize = "/s" + args[1];
            // source file
            srcFile = args[2];
            // outputfolder
            outLoc = args[3];
            // number of images generated
            if (args.Length > 4)
                totalImageCount = Convert.ToInt32(args[4]);
        }

        public ImageHelper(int[,] r, int[,] g, int[,] b, int start, int end, string[] args)
        {
            this.r = r;
            this.g = g;
            this.b = b;

            this.start = start;
            this.end = end;

            // cfloc
            cfLoc = args[0];
            // image size
            imageSize = "/s" + args[1];
            // source file
            srcFile = args[2];
            // outputfolder
            outLoc = args[3];
            // number of images generated
            if (args.Length > 4)
                totalImageCount = Convert.ToInt32(args[4]);
        }

        public void generateImages()
        {
            // Run Context Free
            for (int i = start; i < end; i++)
            {
                if (i % 10 == 0)
                {
                    Console.WriteLine("Generating Image: {0}", i + 1);
                }

                Process process = new Process();
                // Configure the process using the StartInfo properties.
                process.StartInfo.FileName = cfLoc;
                process.StartInfo.Arguments = imageSize + " " + srcFile + " " + outLoc + "\\o" + i.ToString() + ".png";
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                process.WaitForExit();// Waits here for the process to exit.
            }
        }

        public void generateImagesWithSTDIN()
        {
            // Run Context Free
            for (int i = start; i < end; i++)
            {
                if (i % 10 == 0)
                {
                    Console.WriteLine("Generating Image: {0}", i + 1);
                }

                string input = srcFile;

                //Process process = new Process();
                //// Configure the process using the StartInfo properties.
                //process.StartInfo.FileName = cfLoc;
                //process.StartInfo.Arguments = imageSize + " " + srcFile + " " + outLoc + "\\o" + i.ToString() + ".png";
                //process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //process.Start();
                //process.WaitForExit();// Waits here for the process to exit.



                Process process = new Process();
                // Configure the process using the StartInfo properties.
                process.StartInfo.FileName = cfLoc;
                process.StartInfo.Arguments = imageSize + " - " + outLoc + "o" + i.ToString() + ".png";
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                input = input.Replace(@"\\", @"\");
                process.StandardInput.Write(input);
                process.StandardInput.Close();
                string _stdout = process.StandardOutput.ReadToEnd();
                string _stderr = process.StandardError.ReadToEnd();
                process.WaitForExit();// Waits here for the process to exit.

                // debug
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("bedug.txt", true))
                {
                    file.WriteLine(_stderr);
                    file.WriteLine();
                    file.WriteLine(_stdout);
                    file.WriteLine("---------------------");
                }
            }
        }

        public void AnalyzeImages()
        {
            for (int i = start; i < end; i++)
            {
                if (i % 10 == 0)
                {
                    Console.WriteLine("Loading Image: {0}", i);
                }

                string src = outLoc + "\\o" + i.ToString() + ".png";
                try
                {
                    Bitmap bmp = new Bitmap(Image.FromFile(src));
                    for (int x = 0; x < r.GetLength(0); x++)
                    {
                        for (int y = 0; y < r.GetLength(1); y++)
                        {
                            Color c = bmp.GetPixel(x, y);
                            lock (r)
                            {
                                r[x, y] += c.R;
                            }
                            lock (g)
                            {
                                g[x, y] += c.G;
                            }
                            lock (b)
                            {
                                b[x, y] += c.B;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not load image {0}", i);
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 5)
            {
                // cfloc
                string cfLoc = args[0];
                // image size
                string imageSize = "/s" + args[1];
                // source file
                string srcFile = args[2];
                // outputfolder
                string outLoc = args[3];
                // number of images generated
                int imageCount = Convert.ToInt32(args[4]);

                // Create images using Context Free.
                Generate(args);
                // Average Images Pixels
                Analyze(args);

                Console.WriteLine("Complete.");
                //Console.ReadKey();
            }
            else if (args.Length == 4) // read heatmap and update using single image
            {
                // cfloc
                string cfLoc = args[0];
                // image size
                string imageSize = "/s" + args[1];
                // source file
                string srcFile = args[2];
                // outputfolder
                string outLoc = args[3];

                // Create images using Context Free.
                GenerateSingle(args);
                // Average Images Pixels
                AnalyzeSingle(args);

                Console.WriteLine("Complete.");
            }
            else
            {
                Console.WriteLine("Please Provide an ImageSize \"CFDGsrcLocation\" \"outputLocation\"");
                //Console.ReadKey();
                // Generate(200);
                // Analyze(200);
            }
        }

        private static void GenerateSingle(string[] args) 
        {
            ImageHelper ih = new ImageHelper(0, 1, args);
            ih.generateImagesWithSTDIN();
            Console.WriteLine("finished generating");
        }

        private static Bitmap GetCopyImage(string path)
        {
            using (Image im = Image.FromFile(path))
            {
                Bitmap bm = new Bitmap(im);
                return bm;
            }
        }

        private static void AnalyzeSingle(string[] args)
        {
            int imageCount = 2;
            int imageSize = Convert.ToInt32(args[1]);
            // outputfolder
            string outLoc = args[3];

            // Average Pixel Values
            int[,] r = new int[imageSize, imageSize];
            int[,] g = new int[imageSize, imageSize];
            int[,] b = new int[imageSize, imageSize];

            //ImageHelper ih = new ImageHelper(r, g, b, 0, 1, args);
            //ih.AnalyzeImages();

            string src = outLoc + "\\o0.png";
            try
            {
                Bitmap bmp = new Bitmap(Image.FromFile(src));
                src = outLoc + "\\average.png";
                Bitmap bmph = GetCopyImage(src); // new Bitmap(Image.FromFile(src));
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = bmp.GetPixel(x, y);
                        Color c2 = bmph.GetPixel(x, y);
                        lock (r)
                        {
                            r[x, y] = (c.R + c2.R) / imageCount;
                        }
                        lock (g)
                        {
                            g[x, y] = (c.G + c2.G) / imageCount;
                        }
                        lock (b)
                        {
                            b[x, y] = (c.B + c2.B) / imageCount;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not load images");
            }
            Console.WriteLine("finished combining images");

            // save new heatmap
            using (Bitmap image = new Bitmap(imageSize, imageSize))
            {
                // average
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = Color.FromArgb(r[x, y], g[x, y], b[x, y]);
                        image.SetPixel(x, y, c);
                    }
                }
                outLoc = args[3] + "\\average.png";
                image.Save(outLoc, ImageFormat.Png);
                Console.WriteLine("Created average.png");

                // bump map
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = image.GetPixel(x, y);
                        float brightness = c.GetBrightness();
                        int nc = 255 - Convert.ToInt32(Math.Floor(255 * brightness));
                        image.SetPixel(x, y, Color.FromArgb(nc, nc, nc));
                    }
                }
                outLoc = args[3] + "\\bumpmap.png";
                image.Save(outLoc, ImageFormat.Png);
                Console.WriteLine("Created bumpmap.png");

                // heat map
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = image.GetPixel(x, y);
                        float brightness = c.GetBrightness();
                        float h = 0.7f - brightness * 0.7f;
                        ColorRGB hsb = ColorRGB.FromHSL(h, 1, 0.5);
                        Color nc = Color.FromArgb(hsb.R, hsb.G, hsb.B);
                        image.SetPixel(x, y, nc);
                    }
                }
                outLoc = args[3] + "\\heatmap.png";
                Console.WriteLine(outLoc);
                image.Save(outLoc, ImageFormat.Png);
                Console.WriteLine("Created heatmap.png");
            }
        }

        private static void Generate(string[] args)
        {
            int imageCount = Convert.ToInt32(args[4]);

            // Create Threads to Generate Images
            try
            {
                Thread t1 = new Thread(new ThreadStart(new ImageHelper(0, imageCount / 4, args).generateImages));
                Thread t2 = new Thread(new ThreadStart(new ImageHelper(imageCount / 4, 2 * imageCount / 4, args).generateImages));
                Thread t3 = new Thread(new ThreadStart(new ImageHelper(2 * imageCount / 4, 3 * imageCount / 4, args).generateImages));
                Thread t4 = new Thread(new ThreadStart(new ImageHelper(3 * imageCount / 4, imageCount, args).generateImages));

                t1.Start();
                t2.Start();
                t3.Start();
                t4.Start();

                t1.Join();
                t2.Join();
                t3.Join();
                t4.Join();

                Console.WriteLine("{0} images generated.", args[4]);
            }
            catch (ThreadStateException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        // TODO: make multithreaded
        // TODO: CLEAN UP YOUR FUCKING CODE
        private static void Analyze(string[] args)
        {
            int imageCount = Convert.ToInt32(args[4]);
            int imageSize = Convert.ToInt32(args[1]);

            // Average Pixel Values
            int[,] r = new int[imageSize, imageSize];
            int[,] g = new int[imageSize, imageSize];
            int[,] b = new int[imageSize, imageSize];

            // Init threads
            try
            {
                Thread t1 = new Thread(new ThreadStart(new ImageHelper(r, g, b, 0, imageCount/4, args).AnalyzeImages));
                Thread t2 = new Thread(new ThreadStart(new ImageHelper(r, g, b, imageCount / 4, 2 * imageCount / 4, args).AnalyzeImages));
                Thread t3 = new Thread(new ThreadStart(new ImageHelper(r, g, b, 2 * imageCount / 4, 3 * imageCount / 4, args).AnalyzeImages));
                Thread t4 = new Thread(new ThreadStart(new ImageHelper(r, g, b, 3 * imageCount / 4, imageCount, args).AnalyzeImages));

                t1.Start();
                t2.Start();
                t3.Start();
                t4.Start();

                t1.Join();
                t2.Join();
                t3.Join();
                t4.Join();

                Console.WriteLine("{0} images read.", args[4]);
            }
            catch (ThreadStateException e)
            {
                Console.WriteLine(e.ToString());
            }

            // Divide by total image count.
            for (int x = 0; x < r.GetLength(0); x++)
            {
                for (int y = 0; y < r.GetLength(1); y++)
                {
                    r[x, y] /= imageCount;
                    g[x, y] /= imageCount;
                    b[x, y] /= imageCount;
                }
            }

            // Create new Bitmap Average
            using (Bitmap image = new Bitmap(imageSize, imageSize))
            {
                // average
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = Color.FromArgb(r[x,y], g[x,y], b[x,y]);
                        image.SetPixel(x, y, c);
                    }
                }
                string outLoc = args[3] + "\\average.png";
                image.Save(outLoc, ImageFormat.Png);
                Console.WriteLine("Created average.png");

                // bump map
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = image.GetPixel(x, y);
                        float brightness = c.GetBrightness();
                        int nc = 255 - Convert.ToInt32(Math.Floor(255 * brightness));
                        image.SetPixel(x, y, Color.FromArgb(nc, nc, nc));
                    }
                }
                outLoc = args[3] + "\\bumpmap.png";
                image.Save(outLoc, ImageFormat.Png);
                Console.WriteLine("Created bumpmap.png");

                // heat map
                for (int x = 0; x < r.GetLength(0); x++)
                {
                    for (int y = 0; y < r.GetLength(1); y++)
                    {
                        Color c = image.GetPixel(x, y);
                        float brightness = c.GetBrightness();
                        float h = 0.7f - brightness * 0.7f;
                        ColorRGB hsb = ColorRGB.FromHSL(h, 1, 0.5);
                        Color nc = Color.FromArgb(hsb.R, hsb.G, hsb.B);
                        image.SetPixel(x, y, nc);
                    }
                }
                outLoc = args[3] + "\\heatmap.png";
                image.Save(outLoc, ImageFormat.Png);
                Console.WriteLine("Created heatmap.png");
            }
        }
    }
}
