﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprache;
using System.IO;

namespace SimpleCFParser
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("..\\..\\TestFile.txt");
            //var parsed = CFDGParser.Document.Parse(reader.ReadToEnd());
            string str = reader.ReadToEnd();
            //var parsed = CFDGParser.Identifier.Parse(str);
            //Console.WriteLine(parsed);
            //Console.WriteLine();

            CFDGParser.VersionType = Version.New;
            var parsed = CFDGParser.ParseDocument(str);
            //var old = (DocumentOld)parsed; // going to have to do this ufck
            //var parsed = CFDGParser.I.Parse(str);
            Console.WriteLine(parsed);
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
